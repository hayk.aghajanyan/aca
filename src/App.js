import { useCallback } from 'react';
import CustomTable from "./components/CustomTable";
import {removeItems, setFilterMode, toggleRowActive} from "./redux/actions";
import {useDispatch} from "react-redux";

const headers = [
    {
        dataIndex: 'name',
        title: 'Name',
        width: 120,
        sorter: false,
        filterMode: null,
    },
    {
        dataIndex: 'rate',
        title: 'Rating',
        width: 120,
        sorter: true,
        filterMode: null,
    },
    {
        dataIndex: 'popularity',
        title: 'Popularity',
        width: 120,
        sorter: true,
        filterMode: null,
    },
];

const data = [
    {
        id: 1,
        name: 'React',
        rate: 178,
        active: false,
        popularity: 91,
    },
    {
        id: 2,
        name: 'Vue',
        rate: 114,
        active: false,
        popularity: 85,
    },
    {
        id: 3,
        name: 'Angular',
        rate: 143,
        active: false,
        popularity: 81,
    },
    {
        id: 4,
        name: 'PHP',
        rate: 107,
        active: false,
        popularity: 79,
    },
    {
        id: 5,
        name: 'Next',
        rate: 117,
        active: false,
        popularity: 53,
    },
    {
        id: 6,
        name: 'React',
        rate: 178,
        active: false,
        popularity: 91,
    },
    {
        id: 7,
        name: 'Vue',
        rate: 114,
        active: false,
        popularity: 85,
    },
    {
        id: 8,
        name: 'Angular',
        rate: 143,
        active: false,
        popularity: 81,
    },
    {
        id: 9,
        name: 'Express',
        rate: 121,
        active: false,
        popularity: 65,
    },
    {
        id: 10,
        name: 'Next',
        rate: 117,
        active: false,
        popularity: 53,
    },
    {
        id: 11,
        name: 'C#',
        rate: 198,
        active: false,
        popularity: 89,
    },
    {
        id: 12,
        name: 'Vue',
        rate: 114,
        active: false,
        popularity: 85,
    },
    {
        id: 13,
        name: 'C++',
        rate: 163,
        active: false,
        popularity: 68,
    },
    {
        id: 14,
        name: 'C',
        rate: 111,
        active: false,
        popularity: 35,
    },
    {
        id: 15,
        name: 'Swift',
        rate: 147,
        active: false,
        popularity: 63,
    },
    {
        id: 16,
        name: 'React',
        rate: 178,
        active: false,
        popularity: 91,
    },
    {
        id: 17,
        name: 'Vue',
        rate: 114,
        active: false,
        popularity: 85,
    },
    {
        id: 18,
        name: 'PHP',
        rate: 107,
        active: false,
        popularity: 79,
    },
    {
        id: 19,
        name: 'Express',
        rate: 121,
        active: false,
        popularity: 65,
    },
    {
        id: 20,
        name: 'Next',
        rate: 117,
        active: false,
        popularity: 53,
    },
    {
        id: 21,
        name: 'React',
        rate: 178,
        active: false,
        popularity: 91,
    },
    {
        id: 22,
        name: 'Vue',
        rate: 114,
        active: false,
        popularity: 85,
    },
    {
        id: 23,
        name: 'Angular',
        rate: 143,
        active: false,
        popularity: 81,
    },
    {
        id: 24,
        name: 'Express',
        rate: 121,
        active: false,
        popularity: 65,
    },
    {
        id: 25,
        name: 'Next',
        rate: 117,
        active: false,
        popularity: 53,
    },
    {
        id: 26,
        name: 'React',
        rate: 178,
        active: false,
        popularity: 91,
    },
    {
        id: 27,
        name: 'C',
        rate: 111,
        active: false,
        popularity: 35,
    },
    {
        id: 28,
        name: 'Angular',
        rate: 143,
        active: false,
        popularity: 81,
    },
    {
        id: 29,
        name: 'Express',
        rate: 121,
        active: false,
        popularity: 65,
    },
    {
        id: 30,
        name: 'Next',
        rate: 117,
        active: false,
        popularity: 53,
    },
    {
        id: 31,
        name: 'React',
        rate: 178,
        active: false,
        popularity: 91,
    },
    {
        id: 32,
        name: 'Vue',
        rate: 114,
        active: false,
        popularity: 85,
    },
    {
        id: 33,
        name: 'Angular',
        rate: 143,
        active: false,
        popularity: 81,
    },
    {
        id: 34,
        name: 'Express',
        rate: 121,
        active: false,
        popularity: 65,
    },
    {
        id: 35,
        name: 'Next',
        rate: 117,
        active: false,
        popularity: 53,
    },
    {
        id: 36,
        name: 'React',
        rate: 178,
        active: false,
        popularity: 91,
    },
    {
        id: 37,
        name: 'C',
        rate: 111,
        active: false,
        popularity: 35,
    },
    {
        id: 38,
        name: 'Angular',
        rate: 143,
        active: false,
        popularity: 81,
    },
    {
        id: 39,
        name: 'Express',
        rate: 121,
        active: false,
        popularity: 65,
    },
    {
        id: 40,
        name: 'Next',
        rate: 117,
        active: false,
        popularity: 53,
    },
    {
        id: 41,
        name: 'C',
        rate: 111,
        active: false,
        popularity: 35,
    },
    {
        id: 42,
        name: 'Vue',
        rate: 114,
        active: false,
        popularity: 85,
    },
    {
        id: 43,
        name: 'Angular',
        rate: 143,
        active: false,
        popularity: 81,
    },
    {
        id: 44,
        name: 'Express',
        rate: 121,
        active: false,
        popularity: 65,
    },
    {
        id: 45,
        name: 'Next',
        rate: 117,
        active: false,
        popularity: 53,
    },
    {
        id: 46,
        name: 'React',
        rate: 178,
        active: false,
        popularity: 91,
    },
    {
        id: 47,
        name: 'Vue',
        rate: 114,
        active: false,
        popularity: 85,
    },
    {
        id: 48,
        name: 'Angular',
        rate: 143,
        active: false,
        popularity: 81,
    },
    {
        id: 49,
        name: 'Express',
        rate: 121,
        active: false,
        popularity: 65,
    },
    {
        id: 50,
        name: 'PHP',
        rate: 107,
        active: false,
        popularity: 79,
    },
    {
        id: 51,
        name: 'React',
        rate: 178,
        active: false,
        popularity: 91,
    },
    {
        id: 52,
        name: 'C++',
        rate: 163,
        active: false,
        popularity: 68,
    },
    {
        id: 53,
        name: 'C++',
        rate: 163,
        active: false,
        popularity: 68,
    },
    {
        id: 54,
        name: 'Express',
        rate: 121,
        active: false,
        popularity: 65,
    },
    {
        id: 55,
        name: 'Next',
        rate: 117,
        active: false,
        popularity: 53,
    },
    {
        id: 56,
        name: 'PHP',
        rate: 107,
        active: false,
        popularity: 79,
    },
    {
        id: 57,
        name: 'C++',
        rate: 163,
        active: false,
        popularity: 68,
    },
    {
        id: 58,
        name: 'Angular',
        rate: 143,
        active: false,
        popularity: 81,
    },
    {
        id: 59,
        name: 'Express',
        rate: 121,
        active: false,
        popularity: 65,
    },
    {
        id: 60,
        name: 'Next',
        rate: 117,
        active: false,
        popularity: 53,
    },
];

const App = () => {
    const dispatch = useDispatch();

    const onItemClick = useCallback((item) => {
        console.log(item);
        dispatch(toggleRowActive(item));
    }, [])

    const onFilter = useCallback((dataIndex, mode) => {
        dispatch(setFilterMode({dataIndex, mode}));
    }, [])

    const onRemoveItems = useCallback(() => {
        dispatch(removeItems());
    }, [])

    return (
        <div className="app">
            <h2>Press D key to delete chosen rows</h2>
            <h2>Click on header to filter the table</h2>
            <CustomTable
                onItemClick={onItemClick}
                onRemoveItems={onRemoveItems}
                onFilter={onFilter}
                headers={headers}
                data={data}
            />
        </div>
    );
}

export default App;
