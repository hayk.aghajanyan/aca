import { useEffect, useState, memo } from 'react';
import classnames from 'classnames';
import {useDispatch, useSelector} from "react-redux";
import handleViewport from "react-in-viewport/dist/next/lib/handleViewport";
import arrowDown from '../../assets/img/arrow-down-sign-to-navigate.svg'
import {setAllActive, setTableData, setTableHeaders} from "../../redux/actions";

const Tracker = ({ forwardedRef }) => {
    return (
        <div style={{opacity: 0}} ref={forwardedRef}>hidden</div>
    );
}

const ViewPort = handleViewport(Tracker)

const CustomTable = ({onItemClick, onRemoveItems, onFilter, headers, data}) => {
    const {tableData, tableHeaders} = useSelector(({tableReducer}) => {
        const {tableData, tableHeaders} = tableReducer;
        return {
            tableData,
            tableHeaders,
        }
    })
    const dispatch = useDispatch()
    const [isDataOver, setIsDataOver] = useState(false);

    useEffect(() => {
        document.addEventListener('keypress', handleRemove)
        return () => document.removeEventListener('keypress', handleRemove)
    }, [])


    useEffect(() => {
        setDataPart(15);
        dispatch(setTableHeaders(headers));
    }, [])

    const setDataPart = (n) => {
        if (data.length >= n) {
            const dataToSet = data.splice(0, n);
            dispatch(setTableData(dataToSet));
        } else {
            dispatch(setTableData(data));
            setIsDataOver(true);
        }
    }

    const handleRemove = e => {
        if (e.code === 'KeyD') {
            onRemoveItems();
        }
    }

    const sortHandler = (header) => {
        if(header.sorter) {
            const dir = header.filterMode === 'asc'? 'desc' : 'asc';
            onFilter(header.dataIndex, dir);
        }
    }

    const chooseAll = () => {
        dispatch(setAllActive())
    }

    const showMore = () => {
        setDataPart(15)
    }

    return (
        <div className="container">
            <button className="choose-btn" onClick={chooseAll}>choose all</button>
            <div className="table-container">
                <table>
                    <thead>
                    <tr>
                        {tableHeaders.map(header => (
                            <th
                                key={header.dataIndex}
                                style={{width: `${header.width}px`, userSelect: 'none'}}
                                onClick={() => sortHandler(header)}
                            >{header.title}
                                {
                                    header.sorter && header.filterMode &&
                                    <img
                                        src={arrowDown}
                                        alt={'#'}
                                        className={classnames({
                                            rotated: header.filterMode === 'asc',
                                        })}
                                    />
                                }
                            </th>
                        ))}
                    </tr>
                    </thead>
                    <tbody>
                    {tableData && tableData.map((item, i) => {
                        if(i === tableData.length - 1) {
                            return (
                                <tr
                                    className={classnames('body-tr', {
                                        active: item.active,
                                    })}
                                    key={item.id}
                                    onClick={() => onItemClick(item)}

                                >
                                    <td>{item.name}</td>
                                    <td>{item.rate}</td>
                                    <td>{item.popularity}</td>
                                </tr>
                            )
                        } else {
                            return (
                                <tr
                                    className={classnames('body-tr', {
                                        active: item.active,
                                    })}
                                    key={item.id}
                                    onClick={() => onItemClick(item)}
                                >
                                    <td>{item.name}</td>
                                    <td>{item.rate}</td>
                                    <td>{item.popularity}</td>
                                </tr>
                            )
                        }
                    })}
                    </tbody>
                </table>
            </div>
            {isDataOver && <div>Data is over</div>}
            <ViewPort
                onEnterViewport={isDataOver ? null : () => showMore()}
            />
        </div>
    )
}

export default memo(CustomTable);