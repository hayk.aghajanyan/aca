import {
    SET_FILTER_MODE,
    SET_TABLE_DATA,
    TOGGLE_ROW_ACTIVE,
    SET_TABLE_HEADERS,
    REMOVE_ITEMS,
    SET_ALL_ACTIVE
} from "../../constants";

export const setTableData = (payload) => ({
    type: SET_TABLE_DATA,
    payload,
})
export const setTableHeaders = (payload) => ({
    type: SET_TABLE_HEADERS,
    payload,
})

export const toggleRowActive = (payload) => ({
    type: TOGGLE_ROW_ACTIVE,
    payload,
})

export const setFilterMode = (payload) => ({
    type: SET_FILTER_MODE,
    payload,
})

export const removeItems = () => ({
    type: REMOVE_ITEMS,
})
export const setAllActive = () => ({
    type: SET_ALL_ACTIVE,
})


