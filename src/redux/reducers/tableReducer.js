import {
    SET_FILTER_MODE,
    SET_TABLE_DATA,
    SET_TABLE_HEADERS,
    TOGGLE_ROW_ACTIVE,
    REMOVE_ITEMS,
    SET_ALL_ACTIVE
} from "../../constants";

const initialData = {
    tableData: [],
    tableHeaders: [],
}

const tableReducer = (state = initialData, {type, payload}) => {
    switch (type) {
        case SET_TABLE_DATA:
            return {
                ...state,
                tableData: [
                    ...state.tableData,
                    ...payload,
                ]
            }
        case TOGGLE_ROW_ACTIVE:
            payload.active = !payload.active;
            return {
                ...state,
            }
        case SET_ALL_ACTIVE:
            for (const item of state.tableData) {
                item.active = true
            }
            return {
                ...state
            }
        case SET_TABLE_HEADERS:
            return {
                ...state,
                tableHeaders: [
                    ...payload,
                ]
            }
        case SET_FILTER_MODE:
            const filteredHeader = state.tableHeaders.find(header => header.filterMode !== null)
            const currentHeader = state.tableHeaders.find(header => header.dataIndex === payload.dataIndex);
            if (filteredHeader && currentHeader !== filteredHeader) {
                filteredHeader.filterMode = null;
            }
            currentHeader.filterMode = currentHeader.filterMode === 'asc' ? 'desc' : 'asc';
            const sortDir = currentHeader.filterMode === 'asc' ? 1 : -1;
            const sortedArr = state.tableData.sort((a, b) => a[payload.dataIndex] > b[payload.dataIndex]
                ? -1 * sortDir
                : sortDir)

            return {
                ...state,
                tableData: [...sortedArr]
            }
        case REMOVE_ITEMS:
            const remainingRows = state.tableData.filter(row => row.active === false);
            return {
                ...state,
                tableData: [
                    ...remainingRows,
                ]
            }
        default:
            return state
    }
}

export default tableReducer;